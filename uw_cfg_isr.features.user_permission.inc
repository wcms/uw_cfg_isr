<?php

/**
 * @file
 * uw_cfg_isr.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_isr_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer ISR settings'.
  $permissions['administer ISR settings'] = array(
    'name' => 'administer ISR settings',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_cfg_isr',
  );

  return $permissions;
}
