<?php

/**
 * @file
 * uw_cfg_isr.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_isr_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cas_user_register';
  $strongarm->value = 1;
  $export['cas_user_register'] = $strongarm;

  return $export;
}
