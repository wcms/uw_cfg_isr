<?php

/**
 * @file
 * uw_cfg_isr.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_cfg_isr_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_institutional-survey:institutional_survey.
  $menu_links['menu-site-management_isr-settings:admin/config/system/isr_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/isr_settings',
    'router_path' => 'admin/config/system/isr_settings',
    'link_title' => 'ISR Settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_isr-settings:admin/config/system/isr_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Exported menu link: main-menu_institutional-survey:institutional_survey.
  $menu_links['main-menu_institutional-survey:institutional_survey'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'institutional_survey',
    'router_path' => 'institutional_survey',
    'link_title' => 'Complete the social and academic life at Waterloo survey',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_institutional-survey:institutional_survey',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('ISR settings');
  t('Complete the social and academic life at Waterloo survey');

  return $menu_links;
}
