<?php

/**
 * @file
 * Institutional Survey Redirector Sample.
 */

// --------------------------------------
//
// The purpose of this script is to demonstrate how institutions should
// handle a student and correctly send them to CTC's systems.
//
// Please be familiar with the documentation.
//
// * General outline:
//
//     https://docs.google.com/document/d/1KI_4_lPCGbT3z6ET29OoXrjYxnrIwmRS7bVjYVB4gB0/edit#heading=h.ir20816dq49z
//
// * Specific set up and testing:
//
//     https://docs.google.com/document/d/1kNBTpwoA9zT7CC0zHdbEpxj-QPyEn40ZuJ7ARpicTmU/edit
//
// If your institution can run php scripts, you can adapt this sample by
// configuring the settings in the section below.
//
// If translating this sample into another language and using pre-share
// deidentification, it is essential that you test your hashing implementation,
// otherwise it may be impossible to analyze your data. Re-implement the
// functions stripToken() and hashToken() below. Then test each of the string-
// hash pairs in the testHashToken() function, using the salt '1234567890'.
//
// Finally, all institutions should be sure to complete the testing steps
// detailed in the testing document (also linked above):
//
//     https://docs.google.com/document/d/1kNBTpwoA9zT7CC0zHdbEpxj-QPyEn40ZuJ7ARpicTmU/edit
//
//
// Inputs and Outputs
// ------------------
//
// $net_id          str, drawn from institution's sign in system; can be
//                  set manually for debugging by calling the script with
//                  a query string parameter, 'override_token'
//
// No outputs, rather redirects to a CTC-controlled URL containing the
// instutition's cohort code and the student's identification token, which is
// either in plaintext or securely hashed based on the settings below.
// Configuration Settings               ###.
//
// When false, this will send student network ids directly to CTC for
// deidentification after the study.
// To share only deidentified (hashed) tokens with CTC, set this value to true.
$preshare_deidentification = TRUE;

// Retrieve student's institutional network id.
// Exact implementation depends on institution's network.
global $user;
$net_id = $user->name;

/**
 * END of Configuration Settings            ###.
 * The following code should not require modifications. ###.
 */
function hashToken($token, $salt) {
  return hash('sha256', stripToken($token) . $salt);
}

/**
 *
 */
function stripToken($token) {
  return preg_replace('/[^a-z0-9.]/', '', strtolower($token));
}

/**
 *
 */
function testHashToken($salt) {
  // Pairs generated with tools.perts.net/deidentify, using salt '1234567890'.
  $hash_test = array(
    'X{{+16c,@{O9Â¿?'  => '216eae8ae7ec1ca35b25b5ef2cff24f7960678ac270d8b217bcf4c068a1f59b0',
    '|2 )3l opijkQ2'  => 'd38c8857fa53908c54cf7779120bd4fc23fbe637fb3f0fd536f001a61752f5fb',
    'O9kkq<5wem3\\2o' => 'ad9ef209c367c6a3a41ab43fb480492c51efe6200169e9060e98c0a3af93dafd',
    'BBvm$] vt_QB+2'  => '0756cfbef433d64768ae552b8e42a574feeecf74d6dabfa6fd8a90f5fffa88dd',
    '0Gt6+{E/s}?T[>'  => 'c3f396c6b83e5a1f082ca54e4019fdd3cf428bead1b2618fbb02e6e192556a2c',
    ' ,5e`WÃ‚-yxNy6M'  => 'd1fe2721720e96656ae2860517dd2723e4a47e091d57ca79d3ee21d237dab234',
    '6s)Rp`5rv?7:4q'  => '24f23ccef7bdb5e3c6f26e09b080d6a7d6bfc0f63b3bf0e92a1336c8906e59df',
    'w[c*0BPzÂ¿a1jv\'' => 'a4acb255fb7768021166bd9cebf5a8ea8422c3502cb1027243c9328a25f4cedc',
    'wW^<~n"?\\AjQSo' => '5cfec5ba9f498141fa039eac26425f2205ae55ba5d1bb2a7bc91d344b4cb4eeb',
    'Ã‚Ndd=V\'2jh@r_+' => '8be97d240aa591dcfc5992baf1cbf3c60ca549c9661dec38c49d285d44f989f4',
    'bm^!y!2KPF)4h\'' => '5cc72b5ea38d32d464198bffb86255db7360285c4baf438cd8c468f5ad5b6e4a',
    'Q$q,@ qFU"jOw>'  => '20801e417266493c8f8880a34c9620d7d63a5735a8cf36533c7c7ceb51ff1c5b',
    'pC`&"D:Lr0\'%}T' => '3a124773f1232e995e01fc86709c42f2cf238421395fee6fef79b137390e23fe',
    '1uH\'.Wn3EdVFJ{' => 'd64311f39c1967039cee34364e344ab1a4b0ea04bdf29ee8b24f135a2b118d9b',
    'Jb f Ã‚_W?Â¡3RsE'  => '389c5f56a407c7b0afc0b756cce1acb6c5fabd8e00b8c3c7e86cf42035a92bfe',
    'R3~ST7 tar%$#s'  => '35982210962ecd76d2bb5ea42c30b6409716ab2a3e3e1ab89a1f1ceac6a92fb5',
    'soA;A4u7+,8Lm;'  => '879289cfb103385f09999c7e981f49eb3e999a425e7bee42ec143dd20380ae1a',
    '!kW_"9XXl6wm21'  => 'b4eb4809096771cdb31f6048b001d296555a4d5d5a5ed2a16eb42efbd55a69a6',
    'g)-IE|"C: N\'N'  => 'b99d7624b3bac09798fe8f46dfec84229a639b54e9e124ac20d5d5b2069c4431',
    'Â¿4g?=!pH|G2 Â¡b'  => 'c8d713134a3a3e20c940898fe7f4bca46122acc81bc4874322d0695827c19c68',
    'VmÃ‚F%_[SU%Â¡O]>'  => '10cfaffaef5bd9d3a4c5e1d98d9ae2fce043c145721b54b6c1d4fb6e1b70aff6',
    '\'d9^KlXJ9vwucx' => '9f820527b91a04eebe79eae76e4f65c45c59a3d16b58dfe86585baec0147d94b',
    'KePV|lr})> R0\\' => 'add619d670501eb967974aa7c0c45d54435cb277d48ec7a90dc6f34bbb15959b',
    'XY:  ]Z=NqRM4&'  => '66ed2b4ee9799bd92728ea897dc7a54274d5d562ccd4fb4e6ef91cdd6c37ea94',
    'o-q c;bvPc70vz'  => 'f69bf618cc8728c61072cf0713bb336a1b803aa805f1693c3f31d7d914b74d26',
    'z~jQ rILhPfrt#'  => '8a17dcf7e2b1d32397525e318d7df06a322b20330ee2af3699ac1530017f53ec',
    'kS@q_3uiBc{rM~'  => '1131d05f11995a269dbdbb3617e4af4da7adebfbc1a3699cc061f489775a8f40',
    '.ohZp|I}MÂ¿#u4m'  => '69b5f95a65e6e4fa1b43e14fe9990c7e266ebed835876087f64bc516c40bbc51',
    '+*LfMTUOL ,|=o'  => '67d5050dbdcfc8aec1adffad258e9237c874022f562694cea42993a56d1f618a',
    ',?jj[]=i%+/]a.'  => 'e32527a1914988244b51135b8198405c0a5ec91732e915ee007c2c7e78b48846',
    ' ;,U~U>eXt16)\\' => '1641fb2a9000ef7eb73ac4267da983b015117b98de6ea63b4f3a9f86a2dccf49',
    'Â¿U|"F7^WmX}gFg'  => 'c04292e689808f6dae578f3c1bf231e46c7cd3baabab12c75de791bbd0d2a04a',
    'J+UHlCD})Kbq@e'  => '9e9396733f57318c941e2f7d6e5a1c0a71877b0230d76a2a8d5b1a792d09615f',
    '{Â¿*(OZF}0U*]xC'  => 'ada6268b790f79bead3ae2b92e5f62aee72199c0eba0bf4a2c65858ec11d9567',
    'zYC8zw+X&CX1LM'  => 'ab65b9320f7ef75d4df53b042155907411f903f824127e5fe3caada994210b55',
    ' UM@Mf:kKFVwGi'  => 'dce7d6dbdbdd883441613f1574cd7043e6b7d8dbcd3e32c48b95e71b8015d8a8',
    '! L6PXTZH]Y~,T'  => 'e9c265d52fabe1e45e0b4e15b48394153f86136a07e858af4000c57270b30f83',
    ',Ly&m@f Â¿jp G_'  => '8374f81c40d82b65d6cc67eb94831b6a69fe310cbaafbfaef040af32038e5fb8',
    ',[KMjW*:+-^lx('  => 'a78f7664164f5d846519d1a2155ce8baea144b0ad14141cb7c5b68369ee08ab0',
    ' W5vw?A}Â¡&nuj^'  => '39b4023b6a43d484aad2c1d10d2a8a674e0aeca749fb6a7d7d263957c072b911',
    'WjSGOkMdUUpÂ¿ho'  => '9c81ed74b01bdb1faabde5d09239f4e40ddef93c38dd6f9c39612c2e517cb879',
    '85g17b+G^)FB&"'  => 'be618d0ab1d90a5c41a0cfadcfba81891ee088043983a5593fc8500f0e45d2ea',
    '6.,U=uC ]=mC;$'  => 'c4018637f856a2990abe3b490c1d5856812fa31e58fe1f2ddb57f729d784fe74',
    '#Vrjx)EÃ‚90MxmS'  => 'f43eb25a06e4339c535b3836fc4da52b1537c175d9e4ac29c5ef5ba2ae2a44f3',
    'bx1Ã‚)]C/6DA:fx'  => '7c123cb11a0822239a9dbe8ccc6f9d8a4f7262eb29575aab70deb5a4c7e90585',
    'I<A1:DAm#E/`?;'  => 'b2d4a1525a154934bd7347303a0cdeeb415502aef45eb0f577cf8c5fe5829879',
    ' s(..+:+}4AZd#'  => '6460f8e59dfcc669ff23712b6d38516c647296d2adbc93b2aaafc43e0ca5caa5',
    '75n> 6(0 nS8D\'' => 'e29ae8355fc2dfaeafa793ca18821f3e41c946850abad6bca8f4f985afd99c38',
    'y\\OYy3zx:uPmB"' => '6b68d8aabcd117aed61d8aa1b3b3b792047c625e4c5898bf34394d5978007c54',
    '!J- vqmfz;!\'\\A' => '97525284f36a3f7204b0e9db325b8bb34aec198555201a69c5b5b51d3fe66b42',
    ' @&XM)(Ã‚GKIW^z'  => 'a6a8698b59f7dfcdd2f268d0c4d72fe76d6050aa4bea3f6ce0b7d005a3f799fa',
    'fX{]Â¡]l DqFyT"'  => '2fa791e3e0f1ade6bb5780b1f02a19e691bc29f5031057b4c9b490a44228c247',
    'u*4\\O Â¿T3 V!yp' => '355aebd05938379258e1d267c8de987f3b73c82fd3672fba845b7190c0acf4ce',
    '\\Â¡"u.kJW)s14xP' => 'ed612483c5f69611f1f8ecf32af743c415f3bf74db4a96f958e40f68af2ea49d',
    'ZcÃ‚o"-!9 s&x_K'  => '3250ee039b097264c62be6b29e6a356b5323ad407d594d7fd9ae64d636a476f2',
    'qa(eo+ge||~_h'   => 'fe22bc6e2e126d3687f36e00d9de33eafd7e9ff949f342d0b0aabd80a7ff5b00',
    ' g2..n4.[Y^ qD'  => '48eadc42d04aef0c074f0b3b6bf3d09426994f8324c99869d8c510f0fe6f258a',
    'G3 zWKFMDT|ER5'  => '0c81c97b97a3f219c18225f5f52d6d08c427b0e43a6c9b16fd9040c44a119bcc',
    ' Abx$[p.j?v1} '  => '712ef2e82cf5f403b63ba445be4c639e97300f0ca8846afa26a472a1d3c509ca',
    '&Â¡ v[s</_ +@|f'  => 'e546dce6975f8e23a4040b9c19583abf4bec9a6b5c1129160867063fe63c5ea9',
    '  z6SgXsUsS rB'  => 'eb5a523b22c2e0235e4ecfb5c79370deaca77293ee5bcc5956840aa7d2a6cf59',
    'QwGvuE`1a0Ã‚"(W'  => '474b48cd97a654cc73042c0b81dfe46cc18eaf8111a744350c50990503a6985d',
    'uf-mhc|RDMH?%|'  => '0d81612df1adb5bbb36bbdeeafc2218edc220f5b7f4d79ee255a754ac1ee3182',
    't*jm6 Â¿+IVjVQ_'  => 'f782e31e74ff86c699b96b0894dd60e097e76c911f0bb6deeb8d2f3bfd5e113b',
    '_#=Ã‚-uG] $;@CE'  => '7a136576e56f96eaca3dc5cd5a53cf1dc1fac27fab263a364eefb9ef2e31768d',
    'M}"*r1q_]!XD p'  => 'f9141715849503d80794e1a35b7b0f762805fbd73d5788fa45fbb236424faa16',
    '{O DÂ¿}Jk >b_Ih'  => '442ea0d5b2b1bebc184b30299561f2f5287a9f67f6a8ed571ef10f079efdf0e5',
    'W"V)gf2E&6vc$-'  => 'c4f8f9dfea75da1880682514b1a30829f5a2db9a1e9e4519eb6f9171d1e8ddce',
    'h%CFhYa-Â¡)) r'   => 'eaed792106327c7482d08ee60248c89ead1997b33ee00a6b7f2ba361cfed8ec1',
    'Q#sS*Axb=gI>Vz'  => '4a2f08eca999701c33989b76f73f0442028d3d793cfff915038459cd328f5ce9',
    'JbZ;/Tz en!!FQ'  => '8cb8dc3cc7258cac373f37e0bdc56a3c55deb9e247f0fb6d890c2f13d4cc58b2',
    '@/%n6>|l/#=D-T'  => 'defeed4e38651bf0bc378e276c281dd5898f7bee74309b29bba02a44a66d2002',
    'V& 59+Ã‚G{PhHpd'  => 'c58acf4b8226adae63af731c214fc305e35cca519e15332d8d0df4bbff890c64',
    '~uE*Y\\[o A{=Â¿'  => '0630ca7fffda53948f6a5235f225f2a2e7668bd2fc71281d793a5ecd88c48585',
    '3ZX 1N+@ ~&,(z'  => '89ecd153bd2289cf9fddd0a9c1877e5f69e4d4ee74404ec421841da5256872cc',
    '[.HÂ¡02s-_eÂ¡C*'   => 'd8ca5e44541619c4b6ac3cbb7ba74d481a4442e56879db121426de266679d14b',
    '<]mXB. c(aSPej'  => 'b659b9ece10a61db71a472efa9fb5cc54e8b6655df0c2659ba00e414eb16efd2',
    '(;x?wmh9`Gt7q/'  => '72188d7c5927d2fa6311a03e18be271d2610c29ab9f98ef22038566419ccd18d',
    'mNb6+ ]jP~ H 7'  => '0c51b2c124095d797e08199fef671efe9c658d6d122fd587267d171cca85a901',
    '!{lIaÂ¿T>FbJJ.'   => 'dfc0550567388570f0728694818caa6e5b6e8926738bce6dc639e85c7e1efa3f',
    'm_i`< ?qxuBl]\'' => 'ef0031186833e51cff2edce57fdc867afe51ad204b1c58d2eb060785c3a1ff85',
    '.VvÃ‚&Y6Rxo5_aC'  => 'b9b79e3ee4aa04f82f754b2a96ad75be65910430a3d1b51ba92d98594a4543ef',
    ' gN6HahmBxKpjl'  => '2f6a07a92e3ff87dd13d56983ea750155c823ddd4c478c94001ccc927268b512',
    '$yyGvT$wD<&Â¡~q'  => 'f66bf974193159a1e98b25907fd72982c50ca51402d1fb1cafa7b8d164b887a7',
    ')!B/@)}$VqÂ¿7:a'  => '0b9e0dad10147445de6aaacf8036927a7815c51b04a29a2e8ac01c8311593234',
    '9<`sV2@UFU:Rr<'  => '207976c6db9c86e37afd24ae117c4befb5c71502fd5a41adf5761eeec71ac53c',
  );

  $testing_salt = '1234567890';
  foreach ($hash_test as $s => $h) {
    $hashed_token = hashToken($s, $testing_salt);
    if ($hashed_token != $h) {
      throw new Exception("Improperly implemented hash function. The " .
        "following string did not hash correctly: " . $s);
    }
  }

  return TRUE;
}

$prm_base_url = 'https://tools.perts.net/prm';

if (isset($_REQUEST['override_token']) && $_REQUEST['override_token']) {
  // Allow this parameter to override the token for testing.
  $net_id = $_REQUEST['override_token'];
}
else {
  // Make sure the network id has been set and is well formed.
  if (!isset($net_id) || !is_string($net_id) || $net_id == '') {
    throw new Exception("Could not find student's network id.");
  }
}

if ($preshare_deidentification) {
  // Make sure the script's hash function works correctly. Then hash the
  // student's network id with the institution's secret salt.
  testHashToken($salt);
  $token = hashToken($net_id, $salt);
}
else {
  // In post-share de-identification, the identification token IS the net_id.
  $token = $net_id;
}

// Redirect the student to the panel redirection map.
$redirect_url = implode('/', array($prm_base_url, $cohort_code, $token));
header('Location: ' . $redirect_url);
